<?php

namespace App\Http\Controllers;

use App\Models\Watcher;
use App\Http\Requests\StoreWatcherRequest;
use App\Http\Requests\UpdateWatcherRequest;

class WatcherController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreWatcherRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Watcher $watcher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Watcher $watcher)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateWatcherRequest $request, Watcher $watcher)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Watcher $watcher)
    {
        //
    }
}
