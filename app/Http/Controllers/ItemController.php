<?php

namespace App\Http\Controllers;

use App\Models\Detail;
use App\Models\Equipment;
use App\Models\Material;
use App\Models\MeasureUnit;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Item;
use App\Http\Requests\UniversalRequest;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $material = $request->material ?? '';
        $search = $request->search ?? '';
        $found = Item::query();
        if ($search) {
            $found = $found->where(function ($query) use ($search) {
                $query->where('name', 'LIKE', "%{$search}%")
                    ->orWhere('article', 'LIKE', "%{$search}%");
            });
        }
        if ($material) {
            $found = $found->where(function ($query) use ($material) {
                $query->where('material_id', '=', $material);
            });
        }
        $found = $found->where('is_hidden', 0)->orderByRaw('LENGTH(article) ASC')->orderBy('article');
        return Inertia::render('Structure/Index', [
            'items' => $found->paginate(20)->withQueryString()->onEachSide(1),
            'materials' => Material::all(),
            'type' => 'items',
            'search' => $search,
            'material' => $material,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Structure/Setup', [
            'materials' => Material::all(),
            'measure_units' => MeasureUnit::all(),
            'type' => 'item',
            'action' => 'add',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(UniversalRequest $request)
    {
        $item = Item::create([
            'name' => $request->name,
            'article' => $request->article,
            'material_id' => $request->material_id,
            'measure_unit_id' => $request->measure_unit_id,
            'weight' => $request->weight,
            'price_per_unit' => $request->price_per_unit,
            'comment' => $request->comment ?? '',
            'price_date' => date('d-m-Y'),
        ]);
        if ($request->file('image')) {
            $imageName = $item->id . '_' . time() . '.' . $request->image->extension();
            $request->image->move(public_path('photos'), $imageName);
            $item->update([
                'image' => $imageName,
            ]);
        }
        ;
        if ($request->children) {
            $item->update([
                'weight' => null,
                'price_per_unit' => null,
            ]);
            foreach ($request->children as $index => $child) {

                if ($child['amount'] > 0) {
                    if ($child['type'] == 'items') {
                        $item->childrenItems()->attach($child['id'], [
                            'amount' => $child['amount'],
                            'range' => $index,
                        ]);
                    }
                    if ($child['type'] == 'details') {
                        $item->childrenDetails()->attach($child['id'], [
                            'amount' => $child['amount'],
                            'range' => $index,
                        ]);
                    }
                }
            }
        }
        $item->updatePrice(date('d-m-Y'));
        return to_route('items');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $item = Item::findOrFail($id);
        return $item;
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $item = Item::findOrFail($id);
        if ($item->is_hidden == 0) {
            return Inertia::render(
                'Structure/Setup',
                [
                    'item' => Item::findOrFail($id),
                    'materials' => Material::all(),
                    'measure_units' => MeasureUnit::all(),
                    'type' => 'item',
                    'action' => 'edit'
                ]
            );
        } else {
            return to_route('items');
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UniversalRequest $request)
    {
        $item = Item::where('id', '=', $request->id)->first();
        $old_price = $item->getPriceAttribute();
        $item->update([
            'name' => $request->name,
            'article' => $request->article,
            'material_id' => $request->material_id,
            'measure_unit_id' => $request->measure_unit_id,
            'weight' => $request->weight,
            'price_per_unit' => $request->price_per_unit,
            'comment' => $request->comment ?? '',
        ]);
        if ($request->file('image')) {
            $imageName = $item->id . '_' . time() . '.' . $request->image->extension();
            $request->image->move(public_path('photos'), $imageName);
            $item->update([
                'image' => $imageName,
            ]);
        }
        if ($request->children) {
            $item->update([
                'weight' => null,
                'price_per_unit' => null,
            ]);
            $item->childrenItems()->detach();
            $item->childrenDetails()->detach();
            foreach ($request->children as $index => $child) {
                if ($child['amount'] > 0) {
                    if ($child['type'] == 'items') {
                        $item->childrenItems()->attach($child['id'], [
                            'amount' => $child['amount'],
                            'range' => $index,
                        ]);
                    }
                    if ($child['type'] == 'details') {
                        $item->childrenDetails()->attach($child['id'], [
                            'amount' => $child['amount'],
                            'range' => $index,
                        ]);
                    }
                }
            }
        }
        $new_item = Item::where('id', '=', $request->id)->first();
        $new_price = $new_item->getPriceAttribute();
        if ($old_price != $new_price) {
            $new_item->updatePrice(date('d-m-Y'));
        }

        return to_route('items');
    }

    public function copy($id)
    {
        $item = Item::findOrFail($id);
        $copy = $item->replicate();
        $copy->save();
        $getChildrenItems = $item->childrenItems->toArray();
        $getChildrenDetails = $item->childrenDetails->toArray();
        foreach ($getChildrenItems as $childItem) {
            $copy->childrenItems()->attach($childItem['id'], [
                'amount' => $childItem['pivot']['amount'],
            ]);
        }
        foreach ($getChildrenDetails as $childDetail) {
            $copy->childrenItems()->attach($childDetail['id'], [
                'amount' => $childDetail['pivot']['amount'],
            ]);
        }
        return to_route('items.edit', [
            $id => $copy->id,
        ]);
    }

    public function hide(Request $request)
    {
        $item = Item::where('id', $request->id)->first();
        $item->hide();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Item $item)
    {
        //
    }
}
