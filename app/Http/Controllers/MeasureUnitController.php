<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\MeasureUnit;
use App\Http\Requests\SubUniversalRequest;

class MeasureUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Inertia::render('Structure/SubIndex', [
            'items' => MeasureUnit::all(),
            'type' => 'measureunits',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Structure/SubSetup', [
            'type' => 'measureunits',
            'action' => 'add',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(SubUniversalRequest $request)
    {
        MeasureUnit::create(['name' => $request->name, 'short_name' => $request->short_name]);
        return to_route('engi.measureunits');
    }

    /**
     * Display the specified resource.
     */
    public function show(MeasureUnit $measureUnit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        return Inertia::render('Structure/SubSetup', [
            'item' => MeasureUnit::findOrFail($id),
            'type' => 'measureunits',
            'action' => 'edit',
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(SubUniversalRequest $request)
    {
        MeasureUnit::where('id', $request->id)->update(['name' => $request->name, 'short_name' => $request->short_name]);
        return to_route('measureunits');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(MeasureUnit $measureUnit)
    {
        //
    }
}
