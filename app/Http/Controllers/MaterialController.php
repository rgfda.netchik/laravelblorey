<?php

namespace App\Http\Controllers;
use Inertia\Inertia;
use App\Models\Material;
use App\Http\Requests\SubUniversalRequest;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Inertia::render('Structure/SubIndex', [
            'items' => Material::all(),
            'type' => 'materials',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Structure/SubSetup', [
            'type' => 'materials',
            'action' => 'add',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(SubUniversalRequest $request)
    {
        Material::create(['name' => $request->name, 'short_name' => $request->short_name]);
        return to_route('engi.materials');
    }

    /**
     * Display the specified resource.
     */
    public function show(Material $material)
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        return Inertia::render('Structure/SubSetup', [
            'item' => Material::findOrFail($id),
            'type' => 'materials',
            'action' => 'edit',
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(SubUniversalRequest $request)
    {
        Material::where('id', $request->id)->update(['name' => $request->name, 'short_name' => $request->short_name]);
        return to_route('materials');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Material $material)
    {
        //
    }
}
