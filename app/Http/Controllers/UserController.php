<?php

namespace App\Http\Controllers;
use Hash;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Inertia::render('User/Index', [
            'users' => User::all(),
        ]);
    }
    public function add()
    {
        return Inertia::render(
            'User/Add',
            [
                'user' => [],
            ]
        );
    }

    public function create(Request $request)
    {
        $info = [
            'name' => $request->name,
            'email' => $request->email,
            'role_id' => $request->role_id,
            'password' => Hash::make($request->new_password),
        ];
        User::create($info);
        return to_route('users');
    }

    public function edit($id)
    {
        return Inertia::render(
            'User/Edit',
            [
                'user' => User::findOrFail($id),
            ]
        );
    }
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $info = [
            'name' => $request->name,
            'email' => $request->email,
            'role_id' => $request->role_id,
        ];
        if ($request->new_password){
            $info['password'] = Hash::make($request->new_password);
        }
        $user->update($info);
        return to_route('users');
    }
}
