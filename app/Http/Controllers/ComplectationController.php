<?php

namespace App\Http\Controllers;

use App\Models\Complectation;
use App\Models\ComplectationElement;
use App\Models\MeasureUnit;
use App\Models\Material;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Http\Requests\StoreComplectationRequest;
use App\Http\Requests\UpdateComplectationRequest;
use App\Http\Requests\SearchRequest;
use App\Models\User;
use PhpOffice\PhpWord\IOFactory;

class ComplectationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $search = $request->string ?? '';
        $role = auth()->user()->role_id;
        $comps = Complectation::query();
        if ($search){
            $comps = $comps->where(function($query) use ($search){
                $query->where('deal', 'LIKE', "%{$search}%")->orWhere('detail', 'LIKE', "%{$search}%")->orWhere('id', 'LIKE', "%{$search}%");
            });
        }
        if ($role < 3) {
            $comps = $comps->where(function($query) use ($role){
                $query->where('engineer_id', '=', $role)->orWhere('manager_id', '=', $role)->orWhere('watcher_id', '=', $role);
            });
        } else {

        }
        return Inertia::render('Structure/CompIndex', [
            'comps' => $comps->get(),
            'search' => $search,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

        $role = auth()->user()->role_id;
        $info = [
            'status' => 0,
        ];
        if ($role == 1){
            $info['manager_id'] = auth()->user()->id;
        } else {
            $info['engineer_id'] = auth()->user()->id;
        }
        $comp = Complectation::create($info);
        return to_route('comps.edit', [
            'comp' => $comp->id,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreComplectationRequest $request)
    {
        return to_route('complectations');
    }

    /**
     * Display a search of the resource.
     */
    public function search(SearchRequest $request)
    {
        $found = Complectation::where('id', 'LIKE', "%{$request->search}%")->orWhere('reg_number', 'LIKE', "%{$request->search}%")->orWhere('comp_number', 'LIKE', "%{$request->search}%")->get();
        return $found;
    }

    /**
     * Display the specified resource.
     */
    public function show(Complectation $complectation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($comp)
    {
        $comp = Complectation::findOrFail($comp);
        return Inertia::render(
            'Structure/CompSetup',
            [
                'comp' => $comp,
                'users' => User::where('id', '>', 1)->get(),
                'materials' => Material::all(),
                'measure_units' => MeasureUnit::all(),
            ]
        );
    }

    public function copy($comp)
    {
        $item = Complectation::findOrFail($comp);
        $copy = $item->replicate();
        $copy->save();
        $elements = $item->elements->toArray();
        foreach ($elements as $childItem) {
            $copy->elements()->attach($childItem['id'], [
                'amount' => $childItem['pivot']['amount'],
                'range' => $childItem['pivot']['range'],
                'comment' => $childItem['pivot']['comment'],
                'coeff' => $childItem['pivot']['coeff'],
            ]);
        }
        $copy->update(
            ['status' => 0],
        );
        return to_route('comps.edit', [
            'comp' => $copy->id,
        ]);
    }

    public function updateFiles($comp)
    {
        $item = Complectation::findOrFail($comp);
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $settings =
            [
                'marginLeft' => 400,
                'marginRight' => 400,
                'marginTop' => 200,
                'marginBottom' => 200,
            ];
        $styleFirst = [
            'bold' => true,
            'name' => 'Calibri',
            'size' => 12,
        ];
        $styleGetter = [
            'bold' => true,
            'name' => 'Calibri',
            'size' => 12,
        ];

        $styleTitle = [
            'bold' => true,
            'name' => 'Calibri',
            'size' => 12,
        ];
        $styleText = [
            'name' => 'Calibri',
            'size' => 11,
        ];
        $styleObject = [
            'bold' => true,
            'name' => 'Calibri',
            'italic' => true,
            'size' => 12,
        ];
        $styleTableHeader = [
            'bold' => true,
            'name' => 'Calibri',
            'size' => 11,
        ];
        $styleTableContent = [
            'name' => 'Calibri',
            'size' => 11,
        ];
        $section = $phpWord->addSection($settings);
        $header = $section->addHeader(
        );
        $footer = $section->addFooter();
        $header->addImage(public_path('img') . '/header.png', [
            'alignment' => 'center',
            'width' => '555',
        ]);
        $footer->addImage(public_path('img') . '/image4.jpeg', [
            'alignment' => 'center',
            'width' => '555',
        ]);
        $section->addImage(public_path('img') . '/image1.png', [
            'alignment' => 'center',
            'width' => '555',
        ]);
        $section->addText('Исх. №  от ', $styleFirst, [
            'alignment' => 'start',
        ]);
        $section->addText('Получатель:', $styleGetter, [
            'alignment' => 'end',
        ]);
        $section->addText('Коммерческое предложение № ', $styleTitle, [
            'alignment' => 'center',
        ]);
        $section->addText('Компания Блорэй – производитель и поставщик оборудования и услуг в области транспортировки и очистки сточных вод. Наши изделия широко применяются в промышленных, нефтеперерабатывающих предприятиях, ТЦ, АЗС, спорткомплексах, гражданском, дорожном и частном строительстве.', $styleText, [
            'alignment' => 'start',
        ]);
        $section->addText('Объект - ', $styleObject, [
            'alignment' => 'start',
        ]);
        $table = $section->addTable();
        $table->addRow();
        $cell = $table->addCell(1000);
        $cell->addText('№', $styleTableHeader, [
            'alignment' => 'start',
        ]);
        $cell = $table->addCell(4900);
        $cell->addText('Наименование', $styleTableHeader, [
            'alignment' => 'start',
        ]);
        $cell = $table->addCell(1000);
        $cell->addText('Кол-во', $styleTableHeader, [
            'alignment' => 'center',
        ]);
        $cell = $table->addCell(1000);
        $cell->addText('Ед', $styleTableHeader, [
            'alignment' => 'center',
        ]);
        $cell = $table->addCell(1600);
        $cell->addText('Цена <w:br/> (руб. с НДС)', $styleTableHeader, [
            'alignment' => 'center',
        ]);
        $cell = $table->addCell(1600);
        $cell->addText('Сумма <w:br/> (руб. с НДС)', $styleTableHeader, [
            'alignment' => 'center',
        ]);
        $section->addListItem('Срок действия коммерческого предложения 30 дней.');
        $section->addListItem('Цена указана на складе в г. Краснодар.');
        $section->addListItem('Срок изготовления и поставки 2-3 недели.');
        $section->addListItem('График оплаты: оплата производится в размере 70% предоплаты всей стоимости товара. Оставшаяся часть в размере 30% оплачивается за 5 дней до отгрузки товара.');
        $section->addListItem('Настоящее коммерческое предложение выставлено с учетом курса ЕВРО на __.__.____г.   В случае отклонения курса ЕВРО от курса на момент выставления предложения более чем на 3% - предложение требует обновления.');
        $section->addText('Оборудование');
        $writer = IOFactory::createWriter($phpWord, 'Word2007');
        $writer->save(public_path('docs') . '/' . $comp . '.docx');
    }

    public function mail(Request $request, $comp)
    {
        $type = $request->type;
        $format = $request->format;
        $this->updateFiles($comp);
    }

    public function download(Request $request, $comp)
    {
        $type = $request->type;
        $format = $request->format;
        $this->updateFiles($comp);
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateComplectationRequest $request, $comp)
    {
        $complectation = Complectation::findOrFail($comp);
        $complectation->update([
            'status' => $request->status,
            'engineer_id' => $request->engineer_id,
            'manager_id' => $request->manager_id,
            'watcher_id' => $request->watcher_id,
            "deal" => $request->deal,
            "detail" => $request->detail,
        ]);
        $complectation->elements()->detach();
        foreach ($request->elements as $childItem) {
            $complectation->elements()->attach($childItem['id'], [
                'amount' => $childItem['pivot']['amount'],
                'range' => $childItem['pivot']['range'] ?? 0,
                'comment' => $childItem['pivot']['comment'],
                'coeff' => $childItem['pivot']['coeff'],
            ]);
        }
    }

    public function close(Request $request, $comp)
    {
        $complectation = Complectation::findOrFail($comp);
        $complectation->update([
            'status' => $request->status,
            'engineer_id' => $request->engineer_id,
            'manager_id' => $request->manager_id,
            'watcher_id' => $request->watcher_id,
            "deal" => $request->deal,
            "detail" => $request->detail,
        ]);
        $complectation->elements()->detach();
        foreach ($request->elements as $childItem) {
            $complectation->elements()->attach($childItem['id'], [
                'amount' => $childItem['pivot']['amount'],
                'range' => $childItem['pivot']['range'] ?? 0,
                'comment' => $childItem['pivot']['comment'],
                'coeff' => $childItem['pivot']['coeff'],
            ]);
        }
        $complectation->update(
            ['status' => 1],
        );
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Complectation $complectation)
    {
        //
    }

    public function param(Request $request){
        $param = $request->param;
        $value = $request->value;
        $comp = Complectation::findOrFail($request->id);
        $comp->update([
            $param => $value,
        ]);
    }

    public function element(Request $request){
        $comp = Complectation::findOrFail($request->id);
        $comp->elements()->attach($request->item, [
            'amount' => $request->amount ?? 1,
        ]);
    }

    public function pivotAmount(Request $request){
        $target = ComplectationElement::where('id', $request->id)->first();
        $target->update([
            'amount' => $request->amount,
        ]);
    }

    public function pivotCoeff(Request $request){
        $target = ComplectationElement::where('id', $request->id)->first();
        $target->update([
            'coeff' => $request->coeff,
        ]);
    }
    public function pivotComment(Request $request){
        $target = ComplectationElement::where('id', $request->id)->first();
        $target->update([
            'comment' => $request->comment,
        ]);
    }
}
