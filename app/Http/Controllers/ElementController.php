<?php

namespace App\Http\Controllers;

use App\Models\Complectation;
use App\Models\Element;
use App\Http\Requests\StoreElementRequest;
use App\Http\Requests\UpdateElementRequest;
use App\Models\Material;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ElementController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, $param)
    {
        $material = $request->material ?? '';
        $search = $request->search ?? '';
        $deleted = $request->deleted == 1;
        $types = array(
            "customs" => 4,
            "equipments" => 3,
            "details" => 2,
            "items" => 1,
        );
        $type = @$types[$param] ?? 1;
        $found = Element::query();
        if ($search) {
            $found = $found->where(function ($query) use ($search) {
                $query->where('name', 'LIKE', "%{$search}%")
                    ->orWhere('article', 'LIKE', "%{$search}%");
            });
        }
        if ($material) {
            $found = $found->where(function ($query) use ($material) {
                $query->where('material_id', '=', $material);
            });
        }
        if ($deleted) {
            $found = $found->where('is_hidden', '=', 1);
        } else {
            $found = $found->where('is_hidden', '=', 0);
        }
        $found = $found->orderByRaw('LENGTH(article) ASC')->orderBy('article');
        return Inertia::render('Structure/Index', [
            'items' => $found->where('type', '=', $type)->paginate(100)->withQueryString()->onEachSide(1),
            'type' => $param,
            'deleted' => $deleted,
            'search' => $search,
            'material' => $material,
        ]);
    }

    public function hide(Request $request)
    {
        $id = $request->id;
        $item = Element::where('id', '=', $id)->first();
        $item->hide();
    }
    public function restore(Request $request)
    {
        $id = $request->id;
        $item = Element::where('id', '=', $id)->first();
        $item->restore();
    }

    public function destroy(Request $request)
    {
        $id = $request->id;
        $item = Element::where('id', '=', $id)->first();
    }

    public function info(Request $request)
    {
        $id = $request->id;
        $item = Element::where('id', '=', $id)->first();
        return [
            'info' => $item,
        ];
    }

    public function duplicate(Request $request)
    {
        $id = $request->id;
        $item = Element::where('id', '=', $id)->first();
        $copy = $item->replicate();
        $copy->save();
        $children = $item->children->toArray();
        foreach ($children as $child) {
            if ($child['pivot']['amount'] > 0) {
                $copy->children()->attach($child['id'], [
                    'amount' => $child['pivot']['amount'],
                ]);
            }
        }
        return [
            'id' => $copy->id,
        ];
    }

    public function add(Request $request)
    {
        $element = Element::create([
            'name' => $request->name,
            'article' => $request->article ?? null,
            'material_id' => $request->material_id,
            'measure_unit_id' => $request->measure_unit_id,
            'weight' => $request->weight ?? null,
            'comment' => $request->comment ?? null,
            'price_per_unit' => $request->price_per_unit ?? null,
            'type' => $request->type,
        ]);
        if ($request->file('image')) {
            $imageName = $element->id . '_' . time() . '.' . $request->image->extension();
            $request->image->move(public_path('photos'), $imageName);
            $element->update([
                'image' => $imageName,
            ]);
        }
        if ($request->children) {
            $element->update([
                'weight' => null,
                'price_per_unit' => null,
            ]);
            foreach ($request->children as $index => $child) {
                if ($child['amount'] > 0) {
                    $element->children()->attach($child['id'], [
                        'amount' => $child['amount'],
                        'range' => $index,
                    ]);
                }
            }
        }
        $element->changePrice();
        if ($request->parent){
            $parent = Complectation::where('id', $request->parent)->first();
            $parent->elements()->attach($element->id, [
                'amount' => 1,
            ]);
        }

    }

    public function update(Request $request)
    {
        $id = $request->id;
        $item = Element::where('id', '=', $id)->first();
        $oldPrice = $item->getPriceAttribute();
        $item->update([
            'name' => $request->name,
            'article' => $request->article,
            'material_id' => $request->material_id,
            'measure_unit_id' => $request->measure_unit_id,
            'weight' => $request->weight,
            'comment' => $request->comment ?? '',
            'price_per_unit' => $request->price_per_unit,
        ]);
        if ($request->file('image')) {
            $imageName = $item->id . '_' . time() . '.' . $request->image->extension();
            $request->image->move(public_path('photos'), $imageName);
            $item->update([
                'image' => $imageName,
            ]);
        }
        if ($request->children) {
            $item->children()->detach();
            $item->update([
                'weight' => null,
                'price_per_unit' => null,
            ]);
            foreach ($request->children as $index => $child) {
                if ($child['amount'] > 0) {
                    $item->children()->attach($child['id'], [
                        'amount' => $child['amount'],
                        'range' => $index,
                    ]);
                }
            }
        }
        $newPrice = $item->getPriceAttribute();
        if ($oldPrice != $newPrice) {
            $item->changePrice();
        }
    }

    public function search(Request $request)
    {
        $category = $request->category ?? '';
        $material = $request->material ?? '';
        $search = $request->search ?? '';
        $forcomp = $request->forcomp;
        $found = Element::query()->where('is_hidden', '=', 0);
        if ($category) {
            $found = $found->where('type', '=', $category);
        } else {
            $found = $found->where('type', '!=', 4);
        }
        if ($forcomp){
            $found = $found->where('type', '!=', 2);
        }
        if ($material) {
            $found = $found->where('material_id', $material);
        }
        if ($search) {
            $found = $found->where(function ($query) use ($search) {
                $query->where('name', 'LIKE', "%{$search}%")->orWhere('article', 'LIKE', "%{$search}%");
            });
        }
        return $found->orderByRaw('LENGTH(article) ASC')->orderBy('article')->paginate(100);
    }

    public function set(Request $request)
    {
        $param = $request->param;
        $value = $request->value ?? 0;
        $id = $request->id;
        $item = Element::where('id', '=', $id)->first();
        $item->update([
            $param => $value,
        ]);
    }

}
