<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Detail;
use App\Models\Equipment;
use Inertia\Inertia;
use App\Models\Complectation;
use App\Models\User;
use App\Models\Material;
use App\Models\MeasureUnit;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function duplicate(Request $request)
    {
        $type = $request->type;
        $id = $request->id;
        switch ($type) {
            case 'item':
                $item = Item::where('id', $id)->first();
                break;
            case 'detail':
                $item = Detail::where('id', $id)->first();
                break;
            case 'equipment':
                $item = Equipment::where('id', $id)->first();
                break;
        }
        $copy = $item->replicate();
        $copy->save();
        $getChildrenItems = $item->childrenItems->toArray();
        $getChildrenDetails = $item->childrenDetails->toArray();
        foreach ($getChildrenItems as $childItem) {
            if ($childItem['pivot']['amount'] > 0) {
                $copy->childrenItems()->attach($childItem['id'], [
                    'amount' => $childItem['pivot']['amount'],
                ]);
            }
        }
        foreach ($getChildrenDetails as $childDetail) {
            if ($childDetail['pivot']['amount'] > 0) {
                $copy->childrenDetails()->attach($childDetail['id'], [
                    'amount' => $childDetail['pivot']['amount'],
                ]);
            }
        }
        return [
            'id' => $copy->id,
        ];
    }

    public function info(Request $request)
    {
        $type = $request->type;
        $id = $request->id;
        switch ($type) {
            case 'item':
                $item = Item::where('id', $id)->first()->append('price_history');
                break;
            case 'detail':
                $item = Detail::where('id', $id)->first()->append('price_history');
                break;
            case 'equipment':
                $item = Equipment::where('id', $id)->first()->append('price_history');
                break;
        };

        return [
            'info' => $item,
        ];
    }

    public function home()
    {
        return Inertia::render('Main', []);
    }
    public function available()
    {
        $user = auth()->user();
        if ($user->role_id == 3) {
            $complectations = Complectation::all();
        } else {
            $complectations = Complectation::where('manager_id', '=', $user->id)->orWhere('engineer_id', '=', $user->id)->get();
        }
        return Inertia::render('Main/Index', [
            'complectations' => $complectations,
        ]);
    }

    public function add()
    {
        return Inertia::render('Main/Add', [
            'users' => User::all(),
            'materials' => Material::all(),
            'measure_units' => MeasureUnit::all(),
        ]);
    }


    public function edit($id)
    {
        $comp = Complectation::findOrFail($id);
        return Inertia::render('Main/Edit', [
            'comp' => $comp,
            'users' => User::all(),
            'materials' => Material::all(),
            'measure_units' => MeasureUnit::all(),
        ]);
    }
    public function copy($id)
    {
        $comp = Complectation::findOrFail($id);
        return Inertia::render('Main/Copy', [
            'comp' => $comp,
            'users' => User::all(),
            'materials' => Material::all(),
            'measure_units' => MeasureUnit::all(),
        ]);
    }

    public function changeparam(Request $request)
    {
        if ($request->src == 'equipments') {
            Equipment::where('id', '=', $request->item)->update([
                $request->param => $request->value,
            ]);
        }
        if ($request->src == 'details') {
            Detail::where('id', '=', $request->item)->update([
                $request->param => $request->value,
            ]);
        }
        if ($request->src == 'items') {
            Item::where('id', '=', $request->item)->update([
                $request->param => $request->value,
            ]);
            if ($request->param == 'price_per_unit') {
                Item::where('id', '=', $request->item)->update([
                    'price_date' => date('d-m-Y'),
                ]);
            }
        }
    }
}
