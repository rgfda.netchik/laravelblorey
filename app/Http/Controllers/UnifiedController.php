<?php

namespace App\Http\Controllers;

use App\Models\Element;
use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


use App\Models\Item;
use App\Models\Detail;
use App\Models\Equipment;
use App\Models\Custom;

use App\Models\Material;
use App\Models\MeasureUnit;

use App\Models\Image;

use App\Http\Requests\UniversalRequest;
use App\Http\Requests\SearchRequest;

class UnifiedController extends Controller
{
    public function search(Request $request)
    {
        $category = $request->category ?? '';
        $material = $request->material ?? '';
        $search = $request->search ?? '';
        $forcomp = $request->forcomp;
        $tables = ['item'];
        if ($forcomp) {
            array_push($tables, 'equipment');
        } else {
            array_push($tables, 'detail');
        }
        if ($category) {
            $tables = [$category];
        }
        foreach ($tables as $table) {
            if (isset($found)) {
                $oldfound = $found;
            }
            $found = DB::table($table . 's')
                ->selectRaw('id, name, article, image, measure_unit_id, material_id, is_hidden, "' . $table . '" as type')->where('is_hidden', 0);
            if ($material) {
                $found = $found->where('material_id', $material);
            }
            if ($search) {
                $found = $found->where('name', 'LIKE', "%{$search}%")
                    ->orWhere('article', 'LIKE', "%{$search}%");
            }
            if (isset($oldfound)) {
                $found = $oldfound->union($found);
            }
        }
        return $found->orderByRaw('LENGTH(article) ASC')->orderBy('article')->paginate(20);
    }

    public function updatetoels(){
        $items = Item::get();
        $details = Detail::get();
        $equipments = Equipment::get();
        foreach($items as $item){
            $cur = Element::updateOrCreate([
                'type' => '1',
                'old_id' => $item->id,
            ], [
                'name' => $item->name,
                'article' => $item->article,
                'material_id' => $item->material_id,
                'measure_unit_id' => $item->measure_unit_id,
                'weight' => $item->weight,
                'price_per_unit' => $item->price_per_unit,
                'coeff_send' => $item->coeff_send,
                'coeff_sale' => $item->coeff_sale,
                'image' => $item->image,
                'comment' => $item->comment,
                'price_date' => $item->price_date,
                'is_hidden' => $item->is_hidden,
            ]);
            $children_items = $item->childrenItems()->get();
            foreach($children_items as $ch_item){
                $ch_item_element = Element::where('type', '=', 1)->where('old_id', '=', $ch_item->id)->first();
                $cur->children()->syncWithPivotValues($ch_item_element->id, [
                    'amount' => $ch_item->pivot->amount,
                    'range' => $ch_item->pivot->range,
                ], false);
            }
            $children_details = $item->childrenDetails()->get();
            foreach($children_details as $ch_item){
                $ch_item_element = Element::where('type', '=', 2)->where('old_id', '=', $ch_item->id)->first();
                $cur->children()->syncWithPivotValues($ch_item_element->id, [
                    'amount' => $ch_item->pivot->amount,
                    'range' => $ch_item->pivot->range,
                ], false);
            }
        }
        foreach($details as $item){
            $cur = Element::updateOrCreate([
                'type' => '2',
                'old_id' => $item->id,
            ], [
                'name' => $item->name,
                'article' => $item->article,
                'material_id' => $item->material_id,
                'measure_unit_id' => $item->measure_unit_id,
                'weight' => $item->weight,
                'price_per_unit' => $item->price_per_unit,
                'coeff_send' => $item->coeff_send,
                'coeff_sale' => $item->coeff_sale,
                'image' => $item->image,
                'comment' => $item->comment,
                'price_date' => $item->price_date,
                'is_hidden' => $item->is_hidden,
            ]);
            $children_items = $item->childrenItems()->get();
            foreach($children_items as $ch_item){
                $ch_item_element = Element::where('type', '=', 1)->where('old_id', '=', $ch_item->id)->first();
                $cur->children()->syncWithPivotValues($ch_item_element->id, [
                    'amount' => $ch_item->pivot->amount,
                    'range' => $ch_item->pivot->range,
                ], false);
            }
            $children_details = $item->childrenDetails()->get();
            foreach($children_details as $ch_item){
                $ch_item_element = Element::where('type', '=', 2)->where('old_id', '=', $ch_item->id)->first();
                $cur->children()->syncWithPivotValues($ch_item_element->id, [
                    'amount' => $ch_item->pivot->amount,
                    'range' => $ch_item->pivot->range,
                ], false);
            }
        }
        foreach($equipments as $item){
            $cur = Element::updateOrCreate([
                'type' => '3',
                'old_id' => $item->id,
            ], [
                'name' => $item->name,
                'article' => $item->article,
                'material_id' => $item->material_id,
                'measure_unit_id' => $item->measure_unit_id,
                'weight' => $item->weight,
                'price_per_unit' => $item->price_per_unit,
                'coeff_send' => $item->coeff_send,
                'coeff_sale' => $item->coeff_sale,
                'image' => $item->image,
                'comment' => $item->comment,
                'price_date' => $item->price_date,
                'is_hidden' => $item->is_hidden,
            ]);
            $children_items = $item->childrenItems()->get();
            foreach($children_items as $ch_item){
                $ch_item_element = Element::where('type', '=', 1)->where('old_id', '=', $ch_item->id)->first();
                $cur->children()->syncWithPivotValues($ch_item_element->id, [
                    'amount' => $ch_item->pivot->amount,
                    'range' => $ch_item->pivot->range,
                ], false);
            }
            $children_details = $item->childrenDetails()->get();
            foreach($children_details as $ch_item){
                $ch_item_element = Element::where('type', '=', 2)->where('old_id', '=', $ch_item->id)->first();
                $cur->children()->syncWithPivotValues($ch_item_element->id, [
                    'amount' => $ch_item->pivot->amount,
                    'range' => $ch_item->pivot->range,
                ], false);
            }
        }
        return '123';
    }
}
