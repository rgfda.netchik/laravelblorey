<?php

namespace App\Http\Controllers;

use App\Models\Material;
use App\Models\MeasureUnit;
use Inertia\Inertia;
use App\Models\Equipment;
use Illuminate\Http\Request;
use App\Http\Requests\UniversalRequest;

class EquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $material = $request->material ?? '';
        $search = $request->search ?? '';
        $found = Equipment::query();
        if ($search) {
            $found = $found->where(function ($query) use ($search) {
                $query->where('name', 'LIKE', "%{$search}%")
                    ->orWhere('article', 'LIKE', "%{$search}%");
            });
        }
        if ($material) {
            $found = $found->where(function ($query) use ($material) {
                $query->where('material_id', '=', $material);
            });
        }
        $found = $found->where('is_hidden', 0)->orderByRaw('LENGTH(article) ASC')->orderBy('article');
        return Inertia::render('Structure/Index', [
            'items' => $found->paginate(20)->withQueryString()->onEachSide(1),
            'materials' => Material::all(),
            'type' => 'equipments',
            'search' => $search,
            'material' => $material,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Structure/Setup', [
            'materials' => Material::all(),
            'measure_units' => MeasureUnit::all(),
            'type' => 'equipment',
            'action' => 'add'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(UniversalRequest $request)
    {
        $equipment = Equipment::create([
            'name' => $request->name,
            'article' => $request->article,
            'material_id' => $request->material_id,
            'measure_unit_id' => $request->measure_unit_id,
            'weight' => $request->weight,
            'comment' => $request->comment ?? '',
        ]);
        if ($request->file('image')) {
            $imageName = $equipment->id . '_' . time() . '.' . $request->image->extension();
            $request->image->move(public_path('photos'), $imageName);
            $equipment->update([
                'image' => $imageName,
            ]);
        }
        if ($request->children) {

            foreach ($request->children as $index => $child) {


                if ($child['amount'] > 0) {
                    if ($child['type'] == 'items') {
                        $equipment->childrenItems()->attach($child['id'], [
                            'amount' => $child['amount'],
                            'range' => $index,
                        ]);
                    }
                    if ($child['type'] == 'details') {
                        $equipment->childrenDetails()->attach($child['id'], [
                            'amount' => $child['amount'],
                            'range' => $index,
                        ]);
                    }
                }
            }
        }
        $equipment->updatePrice(date('d-m-Y'));
        return to_route('equipments');
    }

    public function show($id)
    {
        $item = Equipment::findOrFail($id);
        return $item;
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $equipment = Equipment::findOrFail($id);
        if ($equipment && !$equipment->is_hidden) {
            return Inertia::render('Structure/Setup', [
                'item' => $equipment,
                'materials' => Material::all(),
                'measure_units' => MeasureUnit::all(),
                'type' => 'equipment',
                'action' => 'edit'
            ]);
        } else {
            return to_route('equipments');
        }
    }

    public function copy($id)
    {
        $item = Equipment::findOrFail($id);
        $copy = $item->replicate();
        $copy->save();
        $getChildrenItems = $item->childrenItems->toArray();
        $getChildrenDetails = $item->childrenDetails->toArray();
        foreach ($getChildrenItems as $childItem) {
            $copy->childrenItems()->attach($childItem['id'], [
                'amount' => $childItem['pivot']['amount'],
            ]);
        }
        foreach ($getChildrenDetails as $childDetail) {
            $copy->childrenDetails()->attach($childDetail['id'], [
                'amount' => $childDetail['pivot']['amount'],
            ]);
        }
        return to_route('equipments.edit', [
            $id => $copy->id,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UniversalRequest $request)
    {
        $item = Equipment::where('id', '=', $request->id)->first();
        $date = (!$request->price_per_unit || $item->price_per_unit == $request->price_per_unit) ? $item->price_date : date('d-m-Y');
        $item->update([
            'name' => $request->name,
            'article' => $request->article,
            'material_id' => $request->material_id,
            'measure_unit_id' => $request->measure_unit_id,
            'weight' => $request->weight,
            'price_per_unit' => $request->price_per_unit,
            'comment' => $request->comment ?? '',
            'price_date' => $date,
        ]);
        if ($request->file('image')) {
            $imageName = $item->id . '_' . time() . '.' . $request->image->extension();
            $request->image->move(public_path('photos'), $imageName);
            $item->update([
                'image' => $imageName,
            ]);
        }
        ;
        if ($request->children) {
            $item->childrenItems()->detach();
            $item->childrenDetails()->detach();
            foreach ($request->children as $index => $child) {
                if ($child['amount'] > 0) {
                    if ($child['type'] == 'items') {
                        $item->childrenItems()->attach($child['id'], [
                            'amount' => $child['amount'],
                            'range' => $index,
                        ]);
                    }
                    if ($child['type'] == 'details') {
                        $item->childrenDetails()->attach($child['id'], [
                            'amount' => $child['amount'],
                            'range' => $index,
                        ]);
                    }
                }
            }
        }
        return to_route('equipments');
    }

    public function hide(Request $request)
    {
        $item = Equipment::where('id', $request->id)->first();
        $item->hide();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Equipment $equipment)
    {
        //
    }
}
