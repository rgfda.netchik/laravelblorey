<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UniversalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "name" => "required|string|max:200",
            "article"  => "nullable|max:200",
            "material_id"  => "required|int|max:200",
            "measure_unit_id"  => "required|int|max:200",
            "weight"  => "sometimes|max:200",
            "price_per_unit"  => "sometimes|max:200",
            "image"  => "nullable",
            "comment"  => "sometimes|nullable|max:200",
            "price_date"  => "sometimes|max:200",
        ];
    }
}
