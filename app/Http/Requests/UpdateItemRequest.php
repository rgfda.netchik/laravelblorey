<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "name" => "required|string|max:200",
            "article"  => "required|string|max:200",
            "material_id"  => "required|int|max:200",
            "measure_unit_id"  => "required|int|max:200",
            "weight"  => "string|max:200",
            "price_per_unit"  => "required|string|max:200",
            "image"  => "file",
            "comment"  => "sometimes|nullable|max:200",
            "price_date"  => "max:200",
        ];
    }
}
