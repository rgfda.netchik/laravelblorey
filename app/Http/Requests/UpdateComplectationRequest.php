<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateComplectationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "manager_id"  => "nullable|int",
            "engineer_id"  => "nullable|int",
            "watcher_id"  => "nullable|int",
            "deal" => "nullable",
            "detail" => "nullable",
            "items" => "nullable",
            "details" => "nullable",
            "equipment" => "nullable",
            "customs" => "nullable",
        ];
    }
}
