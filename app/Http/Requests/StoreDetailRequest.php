<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "name" => "required|string|max:200",
            "article"  => "required|string|max:200",
            "material_id"  => "required|int",
            "measure_unit_id"  => "required|int",
            "weight"  => "required|string|max:200",
            "image_id"  => "nullable",
            "comment"  => "sometimes|nullable|max:200",
            "children" => "required",
        ];
    }
}
