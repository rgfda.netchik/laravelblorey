<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    use HasFactory;

    public $table = 'equipments';
    protected $fillable = [
        "name",
        "article",
        "material_id",
        "measure_unit_id",
        "weight",
        "price_per_unit",
        "coeff_send",
        "coeff_sale",
        "image",
        "comment",
        "price_date",
        "is_hidden",
    ];

    protected $with = [
        "childrenItems",
        "childrenDetails",
    ];

    protected $appends = [
        'price',
        'calc_weight',
        'type',
    ];

    public function getCalcWeightAttribute()
    {
        if ($this->weight > 0) {
            return $this->weight;
        }
        $children_items = $this->childrenItems->SUM(function ($t) {
            return floatval($t->getCalcWeightAttribute()) * $t->pivot->amount;
        });
        $children_details = $this->childrenDetails->SUM(function ($t) {
            return  floatval($t->getCalcWeightAttribute()) * $t->pivot->amount;
        });
        return number_format($children_items + $children_details, 2, '.', ' ');
    }

    public function material()
    {
        return $this->hasOne(Material::class, 'id', 'material_id');
    }
    public function measureunit()
    {
        return $this->hasOne(MeasureUnit::class, 'id', 'measure_unit_id');
    }
    public function childrenItems()
    {
        return $this->belongsToMany(Item::class, 'equipments_items', 'parent_id', 'item_id')->withPivot('amount', 'range', 'id');
    }

    public function childrenDetails()
    {
        return $this->belongsToMany(Detail::class, 'equipments_details', 'parent_id', 'detail_id')->withPivot('amount', 'range', 'id');
    }

    public function getPriceAttribute()
    {
        $children_items = $this->childrenItems->SUM(function ($t) {
            return $t->price * $t->pivot->amount;
        });
        $children_details = $this->childrenDetails->SUM(function ($t) {
            return $t->price * $t->pivot->amount;
        });

        return ceil($children_items + $children_details) ?? 0;
    }

    function getTypeAttribute()
    {
        return 'equipment';
    }

    function getChildrenTotalAttribute()
    {
        return count($this->childrenItems()->get()) + count($this->childrenDetails()->get());
    }

    public function getPriceHistoryAttribute()
    {
        return PriceHistory::where('object_category', 'equipment')->where('object_id', $this->id)->orderBy('date', 'desc')->get();
    }

    public function updatePrice($date)
    {
        $this->update(
            [
                'price_date' => $date,
            ]
        );
        PriceHistory::updateOrCreate(
            [
                'object_category' => 'equipment',
                'object_id' => $this->id,
                'date' => $date,
            ],
            [
                'price' => $this->price,
            ],
        );
        $parentComps = $this->belongsToMany(Complectation::class, 'complectations_equipments', 'parent_id', 'equipment_id', 'equipment_id')->get()->each(function ($parent) use ($date) {
            $parent->updatePrice($date);
        });
        return $this;
    }


    public function hide()
    {
        $this->update(
            [
                'is_hidden' => 1,
            ]
        );
        return $this;
    }
}
