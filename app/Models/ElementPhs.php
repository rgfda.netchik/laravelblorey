<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ElementPhs extends Model
{
    use HasFactory;
    protected $fillable = [
        "element_id",
        "account",
        "date",
        "price",
    ];

    protected $with = [
        'user',
    ];


    public function user() {
        return $this->belongsTo(User::class, 'account');
    }

}
