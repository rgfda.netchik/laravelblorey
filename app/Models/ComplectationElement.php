<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComplectationElement extends Model
{
    use HasFactory;

    public $table = 'complectations_elements';
    protected $fillable = [
        "amount",
        "element_id",
        "parent_id",
        "range",
        "comment",
        "coeff",
    ];

}
