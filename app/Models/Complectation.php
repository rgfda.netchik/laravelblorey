<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Complectation extends Model
{
    use HasFactory;

    public $table = 'complectations';
    protected $fillable = [
        "status",
        "last_version",
        "reg_number",
        "comp_number",
        "engineer_id",
        "manager_id",
        "watcher_id",
        "version",
        "deal",
        "detail",
        "price_date",
    ];

    protected $with = [
        "engineer",
        "manager",
        "watcher",
        "elements",
    ];

    protected $appends = [
        "price",
        "price_history",
        "total_weight",
        "total",
    ];

    public function engineer()
    {
        return $this->hasOne(User::class, 'id', 'engineer_id');
    }
    public function manager()
    {
        return $this->hasOne(User::class, 'id', 'manager_id');
    }
    public function watcher()
    {
        return $this->hasOne(User::class, 'id', 'watcher_id');
    }

    public function elements()
    {
        return $this->belongsToMany(Element::class, 'complectations_elements', 'parent_id', 'element_id')->withPivot('amount', 'range', 'id', 'comment', 'coeff');
    }

    public function getPriceAttribute()
    {
        return $this->elements->SUM(function ($t) {
            return round($t->price * ((100 + $t->pivot->coeff)/100) * $t->pivot->amount, 2);
        });
    }

    public function getPriceHistoryAttribute(){
        return PriceHistory::where('object_category', 'complectations')->where('object_id', $this->id)->orderBy('date', 'desc')->get();
    }

    public function getTotalWeightAttribute(){
        return $this->elements->SUM(function ($t) {
            return $t->calc_weight * $t->pivot->amount;
        });
    }
    public function getTotalAttribute(){
        return $this->elements->SUM(function ($t) {
            return $t->price * $t->pivot->amount;
        });
    }

    public function updatePrice($date)
    {
        $this->update(
            [
                'price_date' => $date,
            ]
        );
        PriceHistory::updateOrCreate(
            [
                'object_category' => 'complectations',
                'object_id' => $this->id,
                'date' => $date,
            ],
            [
                'price' => $this->price,
            ],
        );
        return $this;
    }

}
