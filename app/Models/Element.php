<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Element extends Model
{
    use HasFactory;
    protected $fillable = [
        "name",
        "article",
        "material_id",
        "measure_unit_id",
        "weight",
        "price_per_unit",
        "coeff_send",
        "coeff_sale",
        "image",
        "comment",
        "price_date",
        "is_hidden",
        "type",
        "old_id",
    ];

    protected $with = [
        'children',
        'phs',
    ];

    protected $appends = [
        'price',
        'calc_weight',
    ];

    public function children()
    {
        return $this->belongsToMany(Element::class, 'element_links', 'parent_id', 'child_id')->withPivot('amount', 'range', 'id');
    }

    public function getCalcWeightAttribute()
    {
        if ($this->weight) {
            return $this->weight;
        }
        $children = $this->children->SUM(function ($t) {
            return floatval($t->getCalcWeightAttribute()) * $t->pivot->amount;
        });
        return number_format($children, 2, '.', '');
    }

    public function getPriceAttribute()
    {
        $children = $this->children->SUM(function ($t) {
            return $t->price * $t->pivot->amount;
        });
        $sum = $children;
        return ceil($sum) ? ceil($sum) : $this->price_per_unit;
    }

    public function hide() {
        $this->update(
            [
                'is_hidden' => 1,
            ]
        );
        $parentItems = $this->belongsToMany(Element::class, 'element_links', 'child_id', 'parent_id')->get()->each(function ($parent) {
            $parent->hide();
        });
        return $this;
    }

    public function restore() {
        $this->update(
            [
                'is_hidden' => 0,
            ]
        );
        $children = $this->belongsToMany(Element::class, 'element_links', 'parent_id', 'child_id')->get()->each(function ($child) {
            $child->restore();
        });
        return $this;
    }

    public function phs(){
        return $this->hasMany(ElementPhs::class);
    }

    public function changePrice(){
        $parents = $this->belongsToMany(Element::class, 'element_links', 'child_id', 'parent_id')->get()->each(function($parent) {$parent->changePrice();});
        $price = $this->getPriceAttribute();
        ElementPhs::create([
            'element_id' => $this->id,
            'account' => auth()->user()->id,
            'date' => date('d/m/Y'),
            'price' => $price,
        ]);
    }
}
