<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;
    protected $fillable = [
        "name",
        "article",
        "material_id",
        "measure_unit_id",
        "weight",
        "price_per_unit",
        "coeff_send",
        "coeff_sale",
        "image",
        "comment",
        "price_date",
        "is_hidden",
    ];

    protected $with = [
        "childrenItems",
        "childrenDetails",
    ];
    protected $appends = [
        'price',
        'calc_weight',
        'type',
    ];
    public function material()
    {
        return $this->hasOne(Material::class, 'id', 'material_id');
    }
    public function measureunit()
    {
        return $this->hasOne(MeasureUnit::class, 'id', 'measure_unit_id');
    }

    public function childrenItems()
    {
        return $this->belongsToMany(Item::class, 'items_items', 'parent_id', 'item_id')->withPivot('amount', 'range', 'id');
    }

    public function childrenDetails()
    {
        return $this->belongsToMany(Detail::class, 'items_details', 'parent_id', 'detail_id')->withPivot('amount', 'range', 'id');
    }

    public function getCalcWeightAttribute()
    {
        if ($this->weight) {
            return $this->weight;
        }
        $children_items = $this->childrenItems->SUM(function ($t) {
            return floatval($t->getCalcWeightAttribute()) * $t->pivot->amount;
        });
        $children_details = $this->childrenDetails->SUM(function ($t) {
            return floatval($t->getCalcWeightAttribute()) * $t->pivot->amount;
        });
        return number_format($children_items + $children_details, 2, '.', ' ');
    }

    public function getPriceAttribute()
    {
        $children_items = $this->childrenItems->SUM(function ($t) {
            return $t->price * $t->pivot->amount;
        });
        $children_details = $this->childrenDetails->SUM(function ($t) {
            return $t->price * $t->pivot->amount;
        });
        $sum = $children_items + $children_details;
        return ceil($sum) ? ceil($sum) : $this->price_per_unit;
    }

    function getTypeAttribute()
    {
        return 'item';
    }

    public function getPriceHistoryAttribute(){
        return PriceHistory::where('object_category', 'item')->where('object_id', $this->id)->orderBy('date', 'desc')->get();
    }

    public function updatePrice($date)
    {
        $this->update(
            [
                'price_date' => $date,
            ]
        );
        PriceHistory::updateOrCreate(
            [
                'object_category' => 'item',
                'object_id' => $this->id,
                'date' => $date,
            ],
            [
                'price' => $this->price,
            ],
        );
        $parentItems = $this->belongsToMany(Item::class, 'items_items', 'item_id', 'parent_id')->get()->each(function ($parent) use ($date) {
            $parent->updatePrice($date); });
        $parentDetails = $this->belongsToMany(Detail::class, 'details_items', 'item_id', 'parent_id')->get()->each(function ($parent) use ($date) {
            $parent->updatePrice($date); });
        $parentEquips = $this->belongsToMany(Equipment::class, 'equipments_items', 'item_id', 'parent_id')->get()->each(function ($parent) use ($date) {
            $parent->updatePrice($date); });
        $parentComps = $this->belongsToMany(Complectation::class, 'complectations_items', 'item_id', 'parent_id')->get()->each(function ($parent) use ($date) {
            $parent->updatePrice($date); });
        return $this;
    }

    public function hide() {
        $this->update(
            [
                'is_hidden' => 1,
            ]
        );
        $parentItems = $this->belongsToMany(Item::class, 'items_items', 'item_id', 'parent_id')->get()->each(function ($parent) {
            $parent->hide(); });
        $parentDetails = $this->belongsToMany(Detail::class, 'details_items', 'item_id', 'parent_id')->get()->each(function ($parent) {
            $parent->hide(); });
        $parentEquips = $this->belongsToMany(Equipment::class, 'equipments_items', 'item_id', 'parent_id')->get()->each(function ($parent) {
            $parent->hide(); });
        return $this;
    }
}
