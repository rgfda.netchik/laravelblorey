<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PriceHistory extends Model
{
    use HasFactory;


    public $table = 'price_history';
    protected $fillable = [
        "object_id",
        "object_category",
        "date",
        "price",
    ];
}
