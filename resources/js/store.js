import { reactive } from 'vue';
import { router } from '@inertiajs/vue3';
import axios from 'axios';
export const store = reactive({
    popups: {
        edit: {
            active: 0,
            info: {
                children: [],
                image: null,
                type: null,
            },
            id: 0,
            action: '',
        },
        search: {
            active: 0,
            info: {},
            string: '',
            category: '',
            material: '',
            forcomp: false,
            comp: null,
        },
        custom: {
            active: 0,
            info: {},
            id: 0,
            action: '',
        },
        delete: {
            active: 0,
        }
    },
    names: {
        items: 'Комплектующая',
        details: 'Деталь',
        equipments: 'Оборудование',
    },
    detail: {
        active: 0,
        lesser: 0,
        tabs: ['details', 'items'],
    },
    comp: {
        active: 0,
        lesser: 0,
        tabs: ['equipments', 'details', 'items'],
    },
    coeffsOpen: false,
    pricesOpen: false,
});


/*
Создание, изменение и копирование

edit.active = 1;
Приходит действие открыть попап с типом и ID, true/false - копирование.

Создание:
Ничего
Копирование и изменение:
Если копирование - отправляется запрос на копирование, получается ID, если изменение - ничего
Отправляется запрос на показ, по типу и ID.
Приходит ответ с информацией. Информация добавляется в edit.info

Попап меняется с active на false
При клике на оверлей попап закрывается, edit.info очищается.
*/
function compareRanges(a, b) {
    return a.pivot.range - b.pivot.range;
}
export const setupPopup = async (action, id, type, comp) => {
    store.popups.edit.id = id;
    store.popups.edit.active = 1;
    store.popups.edit.action = action;
    if (['copy', 'edit'].includes(action)) {
        if (action == 'copy') {
            await axios.post('/copy', {
                id: store.popups.edit.id,
            }).then((response) => {
                store.popups.edit.id = response.data.id;
            });
        }
        await axios.post('/info', {
            id: store.popups.edit.id,
        }).then((response) => {
            store.popups.edit.info = response.data.info;
            store.popups.edit.info.type = response.data.info.type;
            let children = response.data.info.children.sort(compareRanges);
            children.map(x => {
                x.amount = x.pivot.amount;
                x.range = +x.pivot.range;
                x.parentid = x.pivot.id;
            })
            store.popups.edit.info.children = children;
        });
    } else {
        store.popups.edit.info.type = type;
        if (type == 4){
            store.popups.edit.info.parent = comp;
        }
    }
    store.popups.edit.active = 2;
}
export const customPopup = async (id) => {
    store.popups.custom.active = 1;
    store.popups.custom.active = 2;
    if (id) {
        store.popups.custom.action = 'edit';
    }
    else {
        store.popups.custom.action = 'add';
    }
}
export const closePopup = () => {
    store.popups.edit.active = 0;
    store.popups.edit.info.type = '';
    store.popups.edit.info = {
        children: [],
    };
    store.popups.edit.id = 0;
    store.popups.edit.info.children = [];
    router.visit(route(route().current(), route().params), {
        preserveScroll: true,
    });
}
export const closeSearch = () => {
    store.popups.search.active = 0;
    store.popups.search.info = {};
    store.popups.search.string = '';
    store.popups.search.category = '';
    store.popups.search.material = '',
    store.popups.search.forcomp = false;
    store.popups.search.comp = null;
}
export const closeCustom = () => {
    store.popups.custom.active = 0;
    store.popups.custom.info = {};
    store.popups.custom.action = '';
}
export const addItem = (item, measure) => {
    let newItem = {};
    newItem.id = item.id;
    newItem.name = item.name;
    newItem.article = item.article;
    newItem.type = item.type;
    newItem.amount = item.amount > 0 ? item.amount : 1;
    newItem.measure_unit = measure;
    store.popups.edit.info.children.push(newItem);
}
export const addItemToComp = (item) => {
    router.post(route('comps.element'), {
        id: store.popups.search.comp,
        amount: item.amount,
        item: item.id,
    })
}
export const searchResults = async (page, comp) => {
    if (comp) {
        store.popups.search.comp = comp;
        store.popups.search.forcomp = true;
    }
    let response = await axios.post(
        route('search'),
        {
            search: store.popups.search.string,
            material: store.popups.search.material,
            category: store.popups.search.category,
            forcomp: store.popups.search.forcomp,
            page: page ?? 1,
        }
    );
    store.popups.search.info = response.data;
    store.popups.search.active = 2;
}
export const setupSearch = async () => {
    searchResults(1);
}
export const compSearch = async (comp) => {
    searchResults(1, comp);
}
export const savePopup = async () => {
    let link = '/element' + (store.popups.edit.action == 'add' ? '' : '/update');
    await axios.post(link, store.popups.edit.info, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    }).then((response) => {
        closePopup();
    })
}

export const saveCustom = async () => {
    closeCustom();
}

export const openItems = reactive({
    inner: {},
});
/*
Поиск
*/
