<?php

use App\Http\Controllers\ElementController;
use App\Http\Controllers\MaterialController;
use App\Http\Controllers\MeasureUnitController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\DetailController;
use App\Http\Controllers\EquipmentController;
use App\Http\Controllers\ComplectationController;
use App\Http\Controllers\CommercialController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UnifiedController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth', 'verified'])->group(function(){
    Route::get('/engineering', function () {
        return redirect('/engineering/items');
    });
    Route::get('/engineering/{param}', [ElementController::class, 'index'])->name('engi.index');
    Route::post('/hide', [ElementController::class, 'hide'])->name('engi.hide');
    Route::post('/restore', [ElementController::class, 'restore'])->name('engi.restore');
    Route::post('/destroy', [ElementController::class, 'destroy'])->name('engi.destroy');
    Route::get('/materials', [MaterialController::class, 'index'])->name('engi.materials');
    Route::get('/measureunits', [MeasureUnitController::class, 'index'])->name('engi.measureunits');
    Route::post('/info', [ElementController::class, 'info']);
    Route::post('/copy', [ElementController::class, 'duplicate']);
    Route::post('/element', [ElementController::class, 'add']);
    Route::post('/element/update', [ElementController::class, 'update']);
    Route::post('/element/set', [ElementController::class, 'set']);
    Route::post('/search', [ElementController::class, 'search'])->name('search');
});

Route::middleware(['auth', 'verified'])->group(function(){
    Route::get('/', [MainController::class, 'home'])->name('home');
    Route::get('/comps', [ComplectationController::class, 'index'])->name('comps');
    Route::post('/comps/param', [ComplectationController::class, 'param'])->name('comps.param');
    Route::post('/comps/element', [ComplectationController::class, 'element'])->name('comps.element');
    Route::get('/comps/add', [ComplectationController::class, 'create'])->name('comps.create');
    Route::get('/comps/add/{comp}', [ComplectationController::class, 'copy'])->name('comps.copy');
    Route::get('/comps/{comp}', [ComplectationController::class, 'edit'])->name('comps.edit');
    Route::post('/comps/{comp}', [ComplectationController::class, 'update'])->name('comps.update');
    Route::post('/comps/close/{comp}', [ComplectationController::class, 'close'])->name('comps.close');
    Route::post('/comps/mail/{comp}', [ComplectationController::class, 'mail'])->name('comps.mail');
    Route::post('/comps/download/{comp}', [ComplectationController::class, 'download'])->name('comps.download');
    Route::post('/comps/pivot/amount', [ComplectationController::class, 'pivotAmount'])->name('comps.pivot.amount');
    Route::post('/comps/pivot/coeff', [ComplectationController::class, 'pivotCoeff'])->name('comps.pivot.coeff');
    Route::post('/comps/pivot/comment', [ComplectationController::class, 'pivotComment'])->name('comps.pivot.comment');
    Route::get('/updation', [UnifiedController::class, 'updatetoels'])->name('updatefull');
});

Route::middleware(['auth', 'verified'])->group(function(){
    Route::get('/users', [UserController::class, 'index'])->name('users');
    Route::get('/users/add', [UserController::class, 'add'])->name('users.add');
    Route::post('/users/create', [UserController::class, 'create'])->name('users.create');
    Route::get('/users/{id}', [UserController::class, 'edit'])->name('users.edit');
    Route::post('/users/{id}', [UserController::class, 'update'])->name('users.update');
});

Route::middleware('auth')->group(function () {
    Route::get('/materials/add', [MaterialController::class, 'create'])->name('materials.add');
    Route::get('/materials/{id}', [MaterialController::class, 'edit'])->name('materials.edit');
    Route::post('/materials', [MaterialController::class, 'store'])->name('material.store');
    Route::post('/materials/update', [MaterialController::class, 'update'])->name('material.update');
});
Route::middleware('auth')->group(function () {
    Route::get('/measureunits/add', [MeasureUnitController::class, 'create'])->name('measureunits.add');
    Route::get('/measureunits/{id}', [MeasureUnitController::class, 'edit'])->name('measureunits.edit');
    Route::post('/measureunits', [MeasureUnitController::class, 'store'])->name('measureunits.store');
    Route::post('/measureunits/update', [MeasureUnitController::class, 'update'])->name('measureunit.update');
});

Route::middleware('auth')->group(function () {
    //Route::post('/search', [UnifiedController::class, 'search'])->name('search');
});

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::middleware('auth')->group(function () {
    Route::post('/changeparam', [MainController::class, 'changeparam']);
});

Route::middleware('auth')->group(function () {
    //Route::post('/info', [MainController::class, 'info']);
    //Route::post('/copy', [MainController::class, 'duplicate']);
});

require __DIR__.'/auth.php';
