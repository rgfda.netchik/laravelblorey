<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('customs', function (Blueprint $table) {

            $table->string('name');
            $table->string('article')->nullable();
            $table->foreignId('material_id')
                ->constrained('materials')->nullable();
            $table->foreignId('measure_unit_id')
                ->constrained('measure_units')->nullable();
            $table->string('weight')->nullable();
            $table->string('price_per_unit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('blcustomsogs', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('article');
            $table->dropColumn('material_id');
            $table->dropColumn('measure_unit_id');
            $table->dropColumn('weight');
            $table->dropColumn('price_per_unit');
        });
    }
};
