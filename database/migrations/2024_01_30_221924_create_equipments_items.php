<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('equipments_items', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('amount');
            $table->string('item_id');
            $table->string('parent_id');
            $table->string('range')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('equipments_items');
    }
};
