<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('details', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->string('article')->nullable();
            $table->string('coeff_send')->default('1');
            $table->string('coeff_sale')->default('1');
            $table->foreignId('material_id')
                ->constrained('materials');
            $table->foreignId('measure_unit_id')
                ->constrained('measure_units');
            $table->string('weight')->nullable();
            $table->string('price_per_unit')->nullable();
            $table->string('image')->nullable();
            $table->string('comment')->nullable();
            $table->string('price_date')->nullable();
            $table->tinyInteger('is_hidden')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('details');
    }
};
