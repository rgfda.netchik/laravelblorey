<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('complectations_customs', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('amount');
            $table->string('custom_id');
            $table->string('parent_id');
            $table->string('range')->nullable();
            $table->string('comment')->nullable();
            $table->string('coeff')->default('1');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('complectations_customs');
    }
};
