<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('complectations', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('status');
            $table->string('last_version')->nullable();
            $table->string('engineer_id')->nullable();
            $table->string('manager_id')->nullable();
            $table->string('watcher_id')->nullable();
            $table->string('version')->nullable();
            $table->string('deal')->nullable();
            $table->string('detail')->nullable();
            $table->string('price_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('complectations');
    }
};
