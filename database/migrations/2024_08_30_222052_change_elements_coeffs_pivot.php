<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('complectations_elements', function (Blueprint $table) {
            $table->string('coeff')->default('0')->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('complectations_elements', function (Blueprint $table) {
            $table->string('coeff')->default('1')->change();
        });
    }
};
