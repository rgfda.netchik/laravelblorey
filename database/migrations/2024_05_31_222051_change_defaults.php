<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('items', function ($table) {
            $table->string('coeff_send')->default('0')->change();
            $table->string('coeff_sale')->default('0')->change();
        });
        Schema::table('details', function ($table) {
            $table->string('coeff_send')->default('0')->change();
            $table->string('coeff_sale')->default('0')->change();
        });
        Schema::table('equipments', function ($table) {
            $table->string('coeff_send')->default('0')->change();
            $table->string('coeff_sale')->default('0')->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function ($table) {
            $table->string('coeff_send')->default('1')->change();
            $table->string('coeff_sale')->default('1')->change();
        });
        Schema::table('details', function ($table) {
            $table->string('coeff_send')->default('1')->change();
            $table->string('coeff_sale')->default('1')->change();
        });
        Schema::table('equipments', function ($table) {
            $table->string('coeff_send')->default('1')->change();
            $table->string('coeff_sale')->default('1')->change();
        });
    }
};
